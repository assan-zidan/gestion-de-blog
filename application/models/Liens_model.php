<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Liens_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $id_article;
		private $nom;
		private $icone;
		private $url;
		
		protected $table = 'liens';






	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		public function setId_article($id_article){
			$this->id_article =$id_article;
		}

		public function setNom($nom){
			$this->nom =$nom;
		}

		public function setIcone($icone){
			$this->icone =$icone;
		}

		public function setUrl($url){
			$this->url =$url;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_article(){
			return $this->id_article;
		}

		public function getNom(){
			return $this->nom;
		}

		public function getIcone(){
			return $this->icone;
		}

		public function getUrl(){
			return $this->url;
		}


}		
