<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redaction extends CI_Controller {

			
	public function index(){
		
		 if (isset($_SESSION['REDACTEUR'])) {
			$this->load->view('REDACTEUR/index');
			$this->load->view('template_al/navigation');
			$this->load->view('REDACTEUR/home');
			$this->load->view('REDACTEUR/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Redaction','formulaireConnexion_r')));
		}
	}

	// fonction qui charge le formulaire de connexion pour un rédacteur

	public function formulaireConnexion_r(){
		
		if (isset($_SESSION['REDACTEUR'])) {
			if (isset($_SESSION['REDACTEUR'])) {
				redirect(site_url(array('Redaction','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Redaction','formulaireConnexion_r')));
			}
		}else{
			$this->load->view('gestion_redacteur/formulaire_connexion_r');
		}
	}

	public function manageConnexion_r(){
		
		if (isset($_POST['email']) && isset($_POST['pwd'])) {
			
			$red = $this->Redacteur->findAllRedBd();
			for ($i=0; $i < $red['total']; $i++) { 
				if ($red[$i]['email'] == $_POST['email'] && $red[$i]['pwd'] == $_POST['pwd']) {
					$z=$red[$i]['id_users'];
					$r=$this->Users->findRedInfos($z);
					$_SESSION['REDACTEUR'] = $red[$i];
					$_SESSION['REDACTEUR']['nom'] = $r['nom'];
					$_SESSION['REDACTEUR']['photo_profil'] = $r['photo_profil'];
				}
			}
			
			if (isset($_SESSION['REDACTEUR'])) {
				redirect(site_url(array('Redaction','index')));
			}else{
				$_SESSION['ERR'] = 'Les parametres recus ne correspondent a aucun rédacteur dans notre Database.<br> <b>Veuillez recommencer SVP</b>';
				redirect(site_url(array('Redaction','formulaireConnexion_r')));
			}
		}else{
			redirect(site_url(array('Redaction','formulaireConnexion_r')));
		}
	}

	// Gestions des rédacteurs


	public function manageRed(){
		
		if (isset($_SESSION['REDACTEUR'])) {
			
			$data['AllRed']=$this->Redacteur->findAllRedBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('REDACTEUR/home');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Redaction','formulaireConnexion_r')));
		}
	}

	public function testExitRed($email){
        $etat=0;
        $data['infoRed']=$this->Redacteur->findAllRedBd();
        if ($data['infoRed']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoRed']['total'] ; $i++) { 
                if ($data['infoRed'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}

	public function addRed(){
		if (isset($_SESSION['REDACTEUR'])) {
			if (isset($_POST)) {
				$etat=$this->testExitRed($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['REDACTEUR']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisi  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez la remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Redacteur->hydrate($data);
					$this->Redacteur->addRed();
					$_SESSION['message_save']="Rédacteur enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Redaction','manageRed')));
					
				}else{
					session_destroy();
					direct(site_url(array('Redaction','formulaireConnexion_r')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Redaction','formulaireConnexion_r')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Redaction','formulaireConnexion_r')));
		}

	}


	public function deconnexion_r(){
		if (isset($_SESSION['REDACTEUR'])) {
			session_destroy();
		}
		redirect(site_url(array('Redaction','index')));
	}
}