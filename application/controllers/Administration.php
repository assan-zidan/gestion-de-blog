<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Administration extends CI_Controller
{




	public function index()
	{

		if (isset($_SESSION['ADMIN'])) {
			$data['niveau']=$this->Users->findniveaUsersBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation',$data);
			$this->load->view('ADMIN/home');
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	// fonction qui charge le formulaire de connexion pour un administrateur
	public function formulaireConnexion()
	{

		if (isset($_SESSION['ADMIN'])) {
			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration', 'index')));
			} else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			$this->load->view('gestion_admin/formulaire_connexion');
		}
	}

	//fonction qui permet a un administrateur de se connecter a sa session
	public function manageConnexion()
	{

		if (isset($_POST['email']) && isset($_POST['pwd'])) {

			$admin = $this->Admin->findAllAdminBd();
			for ($i = 0; $i < $admin['total']; $i++) {
				if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['pwd'] == $_POST['pwd']) {
					$a = $admin[$i]['id_users'];
					$data = $this->Users->findUsersInfos($a);
					print_r($data);
					$_SESSION['ADMIN'] = $data[$i];
					$val = "ok";
					break;
					
				}else{
					$val="non";
				}
			}

			if ($val=="ok") {
				$_SESSION['ADMIN']['nom'] = $data['nom'];
				$_SESSION['ADMIN']['photo_profil'] = $data['photo_profil'];
				$_SESSION['ADMIN']['email'] = $_POST['email'];  
				$_SESSION['ADMIN']['pwd'] = $_POST['pwd']; 
				$_SESSION['ADMIN']['id_users'] = $admin[$i]['id_users'];
				$_SESSION['ADMIN']['id'] = $admin[$i]['id'];
				redirect(site_url(array('Administration', 'index')));
			} else {
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	



	



	// Gestions des utilisateurs

	// Fonction  qui liste tous les uttilisateurs
	public function ListUsers()
	{

		if (isset($_SESSION['ADMIN'])) {

			$data['AllUsers'] = $this->Users->findAllUsersBd();

			$data['niveau']=$this->Users->findniveaUsersBd();
			// print_r($data);
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/listusers',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}
	
	public function testExitAdmin($email){
        $etat=0;
        $data['infoAdmin']=$this->Admin->findAllAdminBd();
        if ($data['infoAdmin']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoAdmin']['total'] ; $i++) { 
                if ($data['infoAdmin'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
    }   


// fonction qui affiche la liste des categories (jael)

	public function ListCategorie()
	{

		if (isset($_SESSION['ADMIN'])) {

			$data['AllCategorie'] = $this->Categorie->findAllCategorieBd();
			// print_r($data);
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/listcategorie',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


// fonction qui permet d'ajouter une categorie 

	public function AddCategorie(){

		if (isset($_SESSION['ADMIN'])) {
			$data['AllCategorie']=$this->Categorie->findAllCategorieBd();

			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/add_categorie',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	// ajout categorie en BD

	public function AddCategories(){
		if (isset($_SESSION['ADMIN'])) {
			if(isset($_POST) and isset($_FILES)){
					print_r($_SESSION['ADMIN']);
				if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['image']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['image']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
						$data['image'] = $config;
					} else {
						$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {
					$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$data['message'] = 'non';
				}		
					$data['nom'] = $_POST['nom'];
					$data['image'] = $config;
					$data['nombre'] =$this->Article->findTotalArticleBd();
					print_r($data['nombre']);
					$data['description'] = $_POST['description'];

					$data['date_time'] = date('Y-m-d H:i:s');
					$this->Categorie->hydrate($data);
					$this->Categorie->addCategories();
					redirect(site_url(array('Administration', 'ListCategorie')));

			}
			
			} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}


	}

	// consultation d'un article

		public function ListArticle()
	{

		if (isset($_SESSION['ADMIN'])) {

			$data['AllArticle'] = $this->Article->findAllArticleBd();
			// print_r($data);
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/listarticle',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function AddArticle(){

		if (isset($_SESSION['ADMIN'])) {
			$data['AllArticle']=$this->Article->findAllArticleBd();

			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/add_article',$data);

// 			$data['AllUser']=$this->Users->findAllUsersBd();
// 			$data['niveau']=$this->Users->findniveaUsersBd();
// 			// print_r($data['niveau']);
// 			$this->load->view('ADMIN/index',$data);
// 			$this->load->view('template_al/navigation',$data);
// 			$this->load->view('ADMIN/user_home',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}


	public function AddArticles(){
		if (isset($_SESSION['ADMIN'])) {
			if(isset($_POST) and isset($_FILES)){
					print_r($_SESSION['ADMIN']);
				if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['image']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['image']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['image']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['nom'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['image']['tmp_name'], 'assets/images/' . $config);
						$data['image'] = $config;
					} else {
						$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {
					$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$data['message'] = 'non';
				}		
					print_r($_POST);
					$data['titre'] = $_POST['titre'];
					$data['image'] = $config;
					$data['contenu'] = $_POST['contenu'];
					$data['date_time'] = date('Y-m-d H:i:s');
					$data['nb_like'] = $config;
					$data['etat'] = $config;
					

					
					$this->Article->hydrate($data);
					$this->Article->addArticles();
					redirect(site_url(array('Administration', 'ListArticle')));

			}
			
			} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}


	}

		


	//fonction qui verifie l'existance de l'email d'un admin 


	// fonction qui ajoute tous les utilisateurs
	public function addUsers()
	{
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				if (isset($_FILES['photo_profil']) and $_FILES['photo_profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['photo_profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['photo_profil']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['photo_profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i') . $_SESSION['ADMIN']['id'];
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'assets/images/' . $config);
						$data['photo_profil'] = $config;
					} else {
						$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {
					$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$data['message'] = 'non';
				}

				$data['nom'] = $_POST['nom'];
				$data['email'] = $_POST['email'];
				$data['niveau'] = $_POST['niveau'];

				$data['date'] = date('Y-m-d H:i:s');
				$this->Users->hydrate($data);
				$data['id_users']=$this->Users->addUser();
				print_r($data);
				if ($_POST['niveau']==1) {
					$data['pwd']=$_POST['pwd'];
					$data['id_users']=$data['id_users'];
					$this->Admin->hydrate($data);
					$this->Admin->addAdmin();
				}elseif($_POST['niveau']==3) {
					$data['pwd']=$_POST['pwd'];
					$data['id_users']=$data['id_users'];
					$this->Redacteur->hydrate($data);
					$this->Redacteur->addRed();
				}else{
					$data['pwd']=$_POST['pwd'];
					$data['id_users']=$data['id_users'];
					$this->Moderateur->hydrate($data);
					$this->Moderateur->addMod();
				}
				$_SESSION['message_save'] = "Administrateurs enregistré avec success !!";
				$_SESSION['success'] = 'ok';
				redirect(site_url(array('Administration', 'ListUsers'))); 
			
			}else {
				session_destroy();
				redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}

	// public function addAdmin(){
	// 	if (isset($_SESSION['ADMIN'])) {
	// 		if (isset($_POST)) {
	// 			$etat=$this->testExitAdmin($_POST['email']);
	// 			if ($etat==0) {
					
					
	// 				$data['email']=$_POST['email'];
	// 				$data['pwd']=$_POST['pwd'];
	// 				$data['id_users']=$_POST['id_users'];
					
					
	// 				$data['date']=date('Y-m-d H:i:s');
	// 				$this->Admin->hydrate($data);
	// 				$this->Admin->addAdmin();
	// 				$_SESSION['message_save']="Administrateurs enregistré avec success !!";
	// 		 		$_SESSION['success']='ok';
	// 		 		// session_destroy();
	// 		 		redirect(site_url(array('Administration','manageConnexion')));
					
	// 			}else{
	// 				session_destroy();
	// 				direct(site_url(array('Administration','formulaireConnexion')));
	// 			}
	// 		}else{
	// 			session_destroy();
	// 			redirect(site_url(array('Administration','formulaireConnexion')));
	// 		}
	// 	}else{
	// 		session_destroy();
	// 		redirect(site_url(array('Administration','formulaireConnexion')));
	// 	}

	// }

	// fonction qui permet d'ajouter un utilisateur
	public function AddUser(){

		if (isset($_SESSION['ADMIN'])) {
			$data['AllUser']=$this->Users->findAllUsersBd();
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/user_home',$data);
			$this->load->view('ADMIN/footer');
		} else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
		}
	}




	// public function addAdmin(){
	// 	if (isset($_SESSION['ADMIN'])) {
	// 		if (isset($_POST)) {
	// 			$etat=$this->testExitAdmin($_POST['email']);
	// 			if ($etat==0) {
					
					
	// 				$data['email']=$_POST['email'];
	// 				$data['pwd']=$_POST['pwd'];
	// 				$data['id_users']=$_POST['id_users'];
					
					
	// 				$data['date']=date('Y-m-d H:i:s');
	// 				$this->Admin->hydrate($data);
	// 				$this->Admin->addAdmin();
	// 				$_SESSION['message_save']="Administrateurs enregistré avec success !!";
	// 		 		$_SESSION['success']='ok';
	// 		 		// session_destroy();
	// 		 		redirect(site_url(array('Administration','manageConnexion')));
					
	// 			}else{
	// 				session_destroy();
	// 				direct(site_url(array('Administration','formulaireConnexion')));
	// 			}
	// 		}else{
	// 			session_destroy();
	// 			redirect(site_url(array('Administration','formulaireConnexion')));
	// 		}
	// 	}else{
	// 		session_destroy();
	// 		redirect(site_url(array('Administration','formulaireConnexion')));
	// 	}

	// }

	public function ModAd(){
		if (isset($_SESSION['ADMIN'])) {
			echo("BONJOUR");
			if (($_POST['nom']!=$_SESSION['ADMIN']['nom']) OR ($_POST['email']!=$_SESSION['ADMIN']['email']) OR ($_POST['pwd']!=$_SESSION['ADMIN']['pwd'])) {
				if (($_POST['nom']!=$_SESSION['ADMIN']['nom'])) {
					$this->Users->hydrate($_POST);
					$this->Users->modUser($_SESSION['ADMIN']['id_users'], $_POST['nom'], $_SESSION['ADMIN']['email']);
					$this->Admin->hydrate($_POST);
					$this->Admin->modAdmin($_SESSION['ADMIN']['id_users'], $_SESSION['ADMIN']['email'], $_SESSION['ADMIN']['pwd']);
				} elseif ($_POST['email']!=$_SESSION['ADMIN']['email']) {
					$this->Users->hydrate($_POST);
					$this->Users->modUser($_SESSION['ADMIN']['id_users'],$_SESSION['ADMIN']['nom'], $_POST['email']);
					$this->Admin->hydrate($_POST);
					$this->Admin->modAdmin($_SESSION['ADMIN']['id_users'], $_POST['email'], $_SESSION['ADMIN']['pwd']);
				}elseif ($_POST['pwd']!=$_SESSION['ADMIN']['pwd']){
					$this->Users->hydrate($_POST);
					$this->Users->modUser($_SESSION['ADMIN']['id_users'], $_SESSION['ADMIN']['nom'], $_SESSION['ADMIN']['email']);
					$this->Admin->hydrate($_POST);
					$this->Admin->modAdmin($_SESSION['ADMIN']['id_users'], $_SESSION['ADMIN']['email'], $_POST['pwd']);
				}else{
				$this->Users->hydrate($_POST);
				$this->Users->modUser($_SESSION['ADMIN']['id_users'], $_POST['nom'], $_POST['email']);
				$this->Admin->hydrate($_POST);
				$this->Admin->modAdmin($_SESSION['ADMIN']['id_users'], $_POST['email'],$_POST['pwd']);
				}
			}else{
				$this->Users->hydrate($_POST);
				$this->Users->modUser($_SESSION['ADMIN']['id_users'], $_SESSION['ADMIN']['nom'], $_SESSION['ADMIN']['email']);
				$this->Admin->hydrate($_POST);
				$this->Admin->modAdmin($_SESSION['ADMIN']['id_users'], $_SESSION['ADMIN']['email'], $_SESSION['ADMIN']['pwd']);
				}
		}else {
			session_destroy();
			redirect(site_url(array('Administration', 'formulaireConnexion')));
			}
		
	}

	//fonction qui deconnecte un admin
	public function deconnexion()
	{
		if (isset($_SESSION['ADMIN'])) {
			session_destroy();
		}
		redirect(site_url(array('Administration', 'index')));
		
	}

	//fonction qui affiche le profil d'un admin
			public function profile(){
				if (isset($_SESSION['ADMIN'])) {
					$this->load->view('ADMIN/index');
					$this->load->view('template_al/navigation');
					$this->load->view('ADMIN/profile');
					$this->load->view('ADMIN/footer');
				} else {

					 redirect(site_url(array('Administration', 'index')));	
				}
				
		
			}
}
