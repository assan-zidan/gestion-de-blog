<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Welcome extends CI_Controller {

		
		public function index()
		{	
			if (isset($_SESSION['ABONNE'])) {
				$this->load->view('WELCOME/header');
				$this->load->view('WELCOME/index');
				$this->load->view('WELCOME/footer');
			} else{
				session_destroy();
				$this->load->view('WELCOME/header');
				$this->load->view('WELCOME/index');
				$this->load->view('WELCOME/footer');
			}	
		}

		// fonction qui charge le formulaire d'inscription pour un abonne
		public function inscription()
		{	
			$this->load->view('WELCOME/header','',false);
			$this->load->view('WELCOME/inscription');
			$this->load->view('WELCOME/footer');
		}




		public function formulaireConnexion_abonne()
	{	
		if (isset($_SESSION['ABONNE'])) {
			redirect(site_url(array('Welcome', 'index')));
		} else {
			session_destroy();
			$this->load->view('WELCOME/header','',false);
			$this->load->view('WELCOME/formulaire_connexion_abonne');
			$this->load->view('WELCOME/footer');
		}
		
	}


		



		//fonction qui permet a un ABONNE de se connecter a sa session
	public function Connexion_abonne()
	{

		if (isset($_POST['email']) && isset($_POST['pwd'])) {

			$abonne = $this->Abonne->findAllAboBd();
			for ($i = 0; $i < $abonne['total']; $i++) {
				if ($abonne[$i]['email'] == $_POST['email'] && $abonne[$i]['pwd'] == $_POST['pwd']) {
					$a = $abonne[$i]['id_users'];
					$b = $abonne[$i]['id'];
					$data = $this->Users->findUsersInfos($a);
					// print_r($abonne);
					// print_r($data);
					$_SESSION['ABONNE'] = $data;
					$val = "ok";
					break;
					
				}else{
					$val="non";
				}
			}

			// print_r($_SESSION['ABONNE']);

			if ($val=="ok") {
				$_SESSION['ABONNE']['nom'] = $data['nom'];
				$_SESSION['ABONNE']['photo_profil'] = $data['photo_profil'];
				$_SESSION['ABONNE']['email'] = $_POST['email'];  //  ICI .........
				$_SESSION['ABONNE']['pwd'] = $_POST['pwd']; //  ICI .............
				$_SESSION['ABONNE']['id'] = $b; //  ICI .............
				redirect(site_url(array('Welcome', 'index')));
			} else {
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun abonne dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Welcome', 'formulaireConnexion_abonne')));
			 }
		} else {
			redirect(site_url(array('Welcome', 'formulaireConnexion_abonne')));
		}
	
	}



		public function session_abonne()
	    {
		
			if (isset($_POST)) {
				if (isset($_FILES['photo_profil']) and $_FILES['photo_profil']['error'] == 0) {
					// Testons si le fichier n'est pas trop gros
					if ($_FILES['photo_profil']['size'] <= 100000000) {
						// Testons si l'extension est autorisée
						$infosfichier = pathinfo($_FILES['photo_profil']['name']);
						$extension_upload = $infosfichier['extension'];

						$config = $_FILES['photo_profil']['name'] . date('d') . '-' . date('m') . '-' . date('Y') . 'a' . date('H') . '-' . date('i');
						$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable . '.' . $extension_upload;
						move_uploaded_file($_FILES['photo_profil']['tmp_name'], 'assets/images/user_profil/' . $config);
						$data['photo_profil'] = $config;
					} else {
						$data['message_save'] = "La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
						$data['message'] = 'non';
					}
				} else {
					$data['message_save'] = "L'image choisie  est endommagée  veuillez le remplacer svp !!";
					$data['message'] = 'non';
				}

				$data['nom'] = $_POST['nom'];
				$data['email'] = $_POST['email'];
				$data['niveau'] = $_POST['niveau'];

				$data['date'] = date('Y-m-d H:i:s');
				$this->Users->hydrate($data);
				$data['id_users']=$this->Users->addUser();
				print_r($data['id_users']);
				if ($_POST['niveau']==4) {
					$data['pwd']=$_POST['pwd'];
					$data['id_users']=$data['id_users'];
					$this->Abonne->hydrate($data);
					$this->Abonne->addAbonne();
				}
				$_SESSION['message_save'] = "Administrateurs enregistré avec success !!";
				$_SESSION['success'] = 'ok';
				$_SESSION['ABONNE']['nom']=$_POST['nom'];
				$_SESSION['ABONNE']['photo_profil']=$data['photo_profil'];
				redirect(site_url(array('Welcome', 'index')));
			
			}else {
				session_destroy();
				redirect(site_url(array('Welcome', 'inscription')));
			}
	    }



	public function blog(){

		$this->load->view('WELCOME/header');
		$this->load->view('WELCOME/blog');
		$this->load->view('WELCOME/footer');
	}

	public function article(){

		$this->load->view('WELCOME/header');
		$this->load->view('WELCOME/article');
		$this->load->view('WELCOME/footer');
	}


	public function article_complet(){

		$this->load->view('WELCOME/header');
		$this->load->view('WELCOME/article_complet');
		$this->load->view('WELCOME/footer');
	}


	public function deconnexion()
	{
		if (isset($_SESSION['ABONNE'])) {
			session_destroy();
		
		}	
		print_r($_SESSION);
		redirect(site_url(array('Welcome', 'index')));
	}

		
}
