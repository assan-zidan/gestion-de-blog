		<section class="container">
			<div class="row">
				<div class="col-lg-offset-4 col-lg-4">
					<div class="form-group">
						<form action="<?php echo site_url(array('Welcome','Connexion_abonne')) ?>" method="post">
							<label class="form-label">Email</label>
							<input type="email" name="email" class="form-control">
							<label class="form-label">Mot de passe</label>
							<input type="password" name="pwd" class="form-control">
							<div class="col-xs-12">
	              				<button type="submit" class="btn btn-primary btn-block btn-flat">Connexion</button>
	           				</div>
						</form>
					</div>
				</div>
			</div>
		</section>