<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class Moderation extends CI_Controller {

			
	public function index(){
		
		 if (isset($_SESSION['MODERATEUR'])) {
			$this->load->view('MODERATEUR/index');
			$this->load->view('template_al/navigation');
			$this->load->view('MODERATEUR/home');
			$this->load->view('MODERATEUR/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Moderation','formulaireConnexion')));
		}
	}

	// fonction qui charge le formulaire de connexion pour un moderateur

	public function formulaireConnexion(){
		
		if (isset($_SESSION['MODERATEUR'])) {
			if (isset($_SESSION['MODERATEUR'])) {
				redirect(site_url(array('Moderation','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Moderation','formulaireConnexion')));
			}
		}else{
			$this->load->view('gestion_moderateur/formulaire_connexion_m');
		}
	}


	// Gestions des Moderateurs


	public function manageMod(){
		
		if (isset($_SESSION['MODERATEUR'])) {
			
			$data['AllMod']=$this->Moderateur->findAllModBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('MODERATEUR/home');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Moderation','formulaireConnexion')));
		}
	}

	public function testExitMod($email){
        $etat=0;
        $data['infoMod']=$this->Moderateur->findAllModBd();
        if ($data['infoMod']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoMod']['total'] ; $i++) { 
                if ($data['infoMod'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}

	public function addMod(){
		if (isset($_SESSION['MODERATEUR'])) {
			if (isset($_POST)) {
				$etat=$this->testExitMod($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['MODERATEUR']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisi  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez la remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Moderateur->hydrate($data);
					$this->Moderateur->addMod();
					$_SESSION['message_save']="Modérateur enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Moderation','manageMod')));
					
				}else{
					session_destroy();
					direct(site_url(array('Moderation','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Moderation','formulaireConnexion')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Moderation','formulaireConnexion')));
		}

	}


	// fonction qui permet a un moderateur de se connecter a sa session
	public function connexion_moderateur()
	{

		if (isset($_POST['email']) && isset($_POST['pwd'])) {

			$moderateur = $this->Moderateur->findAllModBd();
			for ($i = 0; $i < $moderateur['total']; $i++) {
				if ($moderateur[$i]['email'] == $_POST['email'] && $moderateur[$i]['pwd'] == $_POST['pwd']) {
					$a = $moderateur[$i]['id_users'];
					$data = $this->Users->findUsersInfos($a);
					print_r($data);
					$_SESSION['MODERATEUR'] = $data[$i];
					$val = "ok";
					break;
					
				}else{
					$val="non";
				}
			}

			if ($val=="ok") {
				$_SESSION['MODERATEUR']['nom'] = $data['nom'];
				$_SESSION['MODERATEUR']['photo_profil'] = $data['photo_profil'];
				redirect(site_url(array('Moderation', 'session_moderateur')));
			} else {
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Moderation', 'formulaireConnexion')));
			}
		} else {
			redirect(site_url(array('Moderation', 'formulaireConnexion')));
		}
	}


	//fonction qui deconnecte un moderateur
	public function deconnexion()
	{
		if (isset($_SESSION['MODERATEUR'])) {
			session_destroy();
		}
		redirect(site_url(array('Moderation', 'index')));
		
	}




}

?>

