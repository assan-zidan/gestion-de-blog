
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>9</h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
           </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>73<sup style="font-size: 20px">%</sup></h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
           </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>312</h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
           </div>
        </div>
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>9</h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
           </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>73<sup style="font-size: 20px">%</sup></h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
           </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6 hover_click">
           <form method="post" action="<?php echo site_url(array('Administration','')) ?>"></form>
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>312</h3>

              <p>lorem</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
           </div>
        </div>
      </div>
        <div class="register-box-body">
          <p class="login-box-msg">Register a new membership</p>

          <form action="../../index.html" method="post">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Full name">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="email" class="form-control" placeholder="Email">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Retype password">
              <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
        <div class="row">
          <div class="col-xs-8">
                <div class="checkbox icheck">
                  <label>
                      <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> I agree to the <a href="#">terms</a>
                  </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
          </div>
      </form>

      <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>
      </div>

      <a href="login.html" class="text-center">I already have a membership</a>
    </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title" style="text-align: center;">LISTE DES REDACTEURS</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Rédacteurs</th>
                  <th>Date</th>
                  <th>StatuTs</th>
                  <th>COMMENTAIRES</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-success">Approved</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>219</td>
                  <td>Alexander Pierce</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-warning">Pending</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>657</td>
                  <td>Bob Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-primary">Approved</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
                <tr>
                  <td>175</td>
                  <td>Mike Doe</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-danger">Denied</span></td>
                  <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
                </tr>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  
















































