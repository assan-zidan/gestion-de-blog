<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Welcome </title>
  <link href="//fonts.googleapis.com/css2?family=Barlow:ital,wght@0,300;0,600;0,700;1,400&display=swap" rel="stylesheet">
  <?php echo  css('bootstrap.min');?>
  <?php echo  css('style');?>
  <?php echo  css('owl.carousel.min');?>
  <?php echo css('icon-font.min'); ?>
  <?php echo css('font-awesome'); ?>
  <?php echo css('ionicons.min'); ?>
  <?php echo css('app/admin_style'); ?>
  <?php echo css('app/style'); ?> 
 
</head>

<body>
    <header >
        <section>
            <div class="content">
                <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <div class="navbar navbar-expand navbar-light ">
                        <div class="col-md-offset-2 col-md-5 col-sm-3 col-xs-8">
                            <a class="navbar-brand">BLOGKMER</a>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        
                        <div class="col-md-offset- ">
                            <ul class="nav navbar-nav collapse navbar-collapse ">
                                <li class="nav-item"><a class="nav-link" href="<?php echo site_url(array('Welcome','index')); ?>">Welcome</a></li>
                                <li ><a href="<?php echo site_url(array('Welcome','blog')); ?>">Blog</a></li>

                                <?php if (isset($_SESSION['ABONNE'])) { ?>
                                <li class="pull pull-right">
                                    <ul>
                                       <li class="nom"><span ><?php 
                                        echo $_SESSION['ABONNE']['nom'];
                                         ?></span>
                                        </li>
                                        <li class="dropdown nom"><a data-toggle="dropdown" href="#">
                                            <?php echo img($_SESSION['ABONNE']['photo_profil'],'','imge_header') ?> <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="#">Mon profils</a></li>
                                                <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','deconnexion')); ?>">Deconexion</a></li>
                                            </ul>
                                        </li> 
                                    </ul>
                                </li>
                                
                                <?php }else{  ?>

                                <li class="dropdown"><a class="btn btn-default" data-toggle="dropdown" href="#">
                                    Mode anonyme<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','formulaireConnexion_abonne')); ?>"> Connexion</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url(array('Welcome','inscription')); ?>">Creer un Compte</a></li>
                                </ul>
                                </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>  
                </div>

                
    

    