    <section class="container">
        <div class="row">
            <div class="col-lg-6">
                Abonnez vous à notre Newsletter
            </div>
            <div class="col-lg-6">
               <div class="form-group">
                   <form>
                        <div class="input-group">
                            <input type="email" name="">
                            <button type="submit" class="btn btn-primary">s'inscrire</button>
                        </div>
                   </form>
               </div> 
            </div>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-lg-4">
                <h3>Navigation</h3>
                <ul>
                    <li><a href="<?php site_url(array('Welcome','index')) ?>">Accueil</a></li>
                    <li><a href="<?php site_url(array('Welcome','blog')) ?>">Blog</a></li>
                    <li><a href="<?php site_url(array('Welcome','AllArticle')) ?>">Article</a></li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h3>liens des reseaux sociaux</h3>
                
                <a href="#facebook" class="btn btn-primary"><span class="fa fa-facebook"></span></a>
                <a href="#twitter" class="btn btn-primary"><span class="fa fa-twitter"></span></a>
                <a href="#instagram" class="btn btn-primary"><span class="fa fa-instagram"></span></a>
                <a href="#google-plus" class="btn btn-primary"><span class="fa fa-google-plus"></span></a>
                <a href="#linkedin" class="btn btn-primary"><span class="fa fa-linkedin"></span></a>
            </div>
            <div class="col-lg-4">
                <h3>contact</h3>

            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <strong>Copyright &copy; 2021 <a href="">IncH Class</a>.</strong> All rights
            reserved.
        </div>
        
    </section>

    

    <?php 
        echo js('jquery-3.3.1.min');
        echo js('jquery-3.6.0.min');
        echo js('popper.min');
        echo js('bootstrap.min');
        echo js('owl.carousel.min');
        echo js('main');
        echo js('jquery.waypoints.min');
        echo js('jquery.countup');
        
    ?>

</body>

</html>

