<script type="text/javascript">
        var bouton = document.getElementById('edit'),
            input = document.getElementsByTagName('input'),
            label = document.getElementsByClassName('indicate');
            imgProfil = document.getElementById('image_depart');

            // console.log(imgProfil);
        var Zimage = input[0], Znom = input[1], Zprenom = input[2], Zmail = input[3], Zpwd = input[4], reset = input[5], submit = input[6];

        //Situation de depart
        function cache(){  // Mode consultation des infos
            Zimage.setAttribute('disabled','true');
            Zimage.previousElementSibling.removeAttribute('title');
            Znom.setAttribute('disabled','true');
            Zprenom.setAttribute('disabled','true');
            Zmail.setAttribute('disabled','true');
            Zpwd.setAttribute('disabled','true');
            reset.style.display='none';
            submit.style.display='none';
            bouton.style.display='inline-block';
            Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
            Zimage.previousElementSibling.appendChild(imgProfil);
            for (var i = label.length - 1; i >= 0; i--) {
                label[i].style.display='none';
            // label[i].style.color='red';
            }
        }

        function affiche (){  // Mode Edition des infos
            Zimage.removeAttribute('disabled');
            Zimage.previousElementSibling.setAttribute('title','Editer');
            Znom.removeAttribute('disabled');
            Zprenom.removeAttribute('disabled');
            Zmail.removeAttribute('disabled');
            Zpwd.removeAttribute('disabled');
            reset.style.display='inline-block';
            submit.style.display='inline-block';
            bouton.style.display='none';
            for (var i = label.length - 1; i >= 0; i--) {
                label[i].style.display='block';
                label[i].setAttribute('title','Editer');
            // label[i].style.color='red';
            }
        }

        function view_newimage(){
            // Supprimons le contenu de la zone d'affichage
            while(Zimage.previousElementSibling.firstChild) {
                Zimage.previousElementSibling.removeChild(Zimage.previousElementSibling.firstChild);
            }
            //
            var image = document.createElement('img'); //on cree in noeud, un element, une balise image
            image.setAttribute('class','img_visualise'); // on attribue a notre balise une classe
            image.src = URL.createObjectURL(Zimage.files[0]); // on recupere le chemin indicant la source de notre image
            Zimage.previousElementSibling.appendChild(image); // on definit notre image comme element enfant de sa zone d'affichage soit on positionne notre noeud dans le dom
        }

        function editable(){

            affiche();
            Zimage.addEventListener('change', view_newimage);
        }
        //on se met en mode consultation
        cache();

        bouton.addEventListener('click', affiche); // on active le mode edition.
        reset.addEventListener('click', cache); // on reviens en mode consultation.
        // submit.addEventListener('click', cache); // on reviens en mode consultation.
        // console.log(label);
        // console.log(Znom);
        // Znom.removeAttribute('disabled');
        // console.log(Znom);    
    </script>