        <div class="mediar_header">
          
        </div>
      </div>
    </section>   
  </header>
  <!-- fin du header -->
  <!-- section de nos informations -->
    <section class="">
      <div class="container ">
        <div class="row">
          <div class="col-lg-6">
            <div class="row"> 
              <?php  echo img('banner1.jpg','','wel_padding');?>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="row">
              <h1>Qui sommes nous</h1>
            </div>
            <div class="row texto">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- fin de section -->
    <!-- section des articles preferes  -->
    <section class="container-fluid">
      <div class="row">
        <h1 class="head"> Articles populaires</h1>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <div class="cardre_article">
            <div class="cardre_image">
              
            </div>
            <h3>titre de l'article</h3>
            <button class="btn btn-primary">Lire</button>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="cardre_article">
            <div class="cardre_image">
              
            </div>
            <h3>titre de l'article</h3>
            <button class="btn btn-primary">Lire</button>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="cardre_article">
            <div class="cardre_image">
              
            </div>
            <h3>titre de l'article</h3>
            <button class="btn btn-primary">Lire</button>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="cardre_article">
            <div class="cardre_image">
              
            </div>
            <h3>titre de l'article</h3>
            <button class="btn btn-primary">Lire</button>
          </div>
        </div>
      </div>
    </section>
    <!-- fin de section -->
    <!-- section des redacteurs-->
    <section class="container">
      <div class="row">
        <h1> Nos Redacteur</h1>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="cardre_redac">
            <div class="row image_title">
              <!-- image -->
            </div>
            <div class="row">
              <h3>nom du redacteur</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="cardre_redac">
            <div class="row image_title">
              <!-- image -->
            </div>
            <div class="row">
              <h3>nom du redacteur</h3>
            </div>
         </div>
        </div>
        <div class="col-lg-4">
          <div class="cardre_redac">
            <div class="row image_title">
             <!-- image -->
            </div>
            <div class="row">
              <h3>nom du redacteur</h3>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="container">
      <div class="row">
        <h1>feedback</h1>
      </div>
      <div class="row">
        <div class="col-lg-4 cardre_feed">
          <div class="profil_ab">
            <div class="row clift">
              bonjour
            </div>
            <div class="row">
              <div class="col-lg-5"></div>
              <div class="col-lg-7 name_ab">
                <h4>auteur</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 cardre_feed">
          <div class="profil_ab">
            <div class="row clift">
              bonjour
            </div>
            <div class="row">
              <div class="col-lg-5"></div>
              <div class="col-lg-7 name_ab">
                <h4>auteur</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 cardre_feed">
          <div class="profil_ab">
            <div class="row clift">
              bonjour
            </div>
            <div class="row">
              <div class="col-lg-5"></div>
              <div class="col-lg-7 name_ab">
                <h4>auteur</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    

  