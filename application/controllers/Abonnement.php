<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abonnement extends CI_Controller {


		
			
	public function index(){
		
		 if (isset($_SESSION['ABONNE'])) {
			$this->load->view('ABONNE/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ABONNE/home');
			$this->load->view('ABONNE/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Abonnement','formulaireConnexion_a')));
		}
	}
	 		
	
	// fonction qui charge le formulaire de connexion pour un abonné
	public function formulaireConnexion_a(){
		
		if (isset($_SESSION['ABONNE'])) {
			if (isset($_SESSION['ABONNE'])) {
				redirect(site_url(array('Abonnement','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Abonnement','formulaireConnexion_a')));
			}
		}else{
			$this->load->view('gestion_abonne/formulaire_connexion_a');
		}
	}
	
	public function manageConnexion_a(){
		
		if (isset($_POST['email']) && isset($_POST['pwd'])) {
			
			$abonne = $this->Abonne->findAllAboBd();
			for ($i=0; $i < $abonne['total']; $i++) { 
				if ($abonne[$i]['email'] == $_POST['email'] && $abonne[$i]['pwd'] == $_POST['pwd']) {
					$z=$abonne[$i]['id_users'];
					$y=$this->Users->findAboInfos($z);
					$_SESSION['ABONNE'] = $abonne[$i];
					$_SESSION['ABONNE']['nom'] = $y['nom'];
					$_SESSION['ABONNE']['photo_profil'] = $y['photo_profil'];
				}
			}
			
			if (isset($_SESSION['ABONNE'])) {
				redirect(site_url(array('Abonnement','index')));
			}else{
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Abonnement','formulaireConnexion_a')));
			}
		}else{
			redirect(site_url(array('Abonnement','formulaireConnexion_a')));
		}
	}

	// Gestions des Abonnés


	public function manageAbo(){
		
		if (isset($_SESSION['ABONNE'])) {
			
			$data['AllAbo']=$this->Abonne->findAllAboBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ABONNE/home_admin');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Abonnement','formulaireConnexion_a')));
		}
	}
	
	public function testExitAbo($email){
        $etat=0;
        $data['infoAbo']=$this->Abonne->findAllAboBd();
        if ($data['infoAbo']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoAbo']['total'] ; $i++) { 
                if ($data['infoAbo'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}
	

	public function addAbo(){
		if (isset($_SESSION['ABONNE'])) {
			if (isset($_POST)) {
				$etat=$this->testExitAbo($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ABONNE']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Abonne->hydrate($data);
					$this->Abonne->addAbo();
					$_SESSION['message_save']="Abonné enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Abonnement','manageAbo')));
					
				}else{
					session_destroy();
					direct(site_url(array('Abonnement','formulaireConnexion_a')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Abonnement','formulaireConnexion_a')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Abonnement','formulaireConnexion_a')));
		}

	}


	public function deconnexion_a(){
		if (isset($_SESSION['ABONNE'])) {
			session_destroy();
		}
		redirect(site_url(array('Abonnement','index')));
	}

	
	//public function desabonnement(){
	//	if (isset($_SESSION['ABONNE'])) {
	//		session_destroy();
	//	}
	//	redirect(site_url(array('Abonnement','page_accueil')));
	//}


	
}
