<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Commentaire_model extends CI_Model{
	function __construct()
		{
		
		}

		private $id;
		private $id_users;
		private $contenu;
		private $date_time;
		
		protected $table = 'Commentaire';






	// definition des getteurs et des setteurs


		// setteurs

		public function setId($id){
			$this->id = $id;
		}

		public function setId_users($id_users){
			$this->id_users =$id_users;
		}

		public function setContenu($contenu){
			$this->contenu =$contenu;
		}

		public function setDate_time($date_time){
			$this->date_time =$date_time;
		}


		// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_users(){
			return $this->id_users;
		}

		public function getContenu(){
			return $this->contenu;
		}

		public function getDate_time(){
			return $this->date_time;
		}


}		